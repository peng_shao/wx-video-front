// components/comments-view/comments-view.js
const utils = require('../../utils/utils.js');

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    videoUserId: {
      type: String,
      value: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    appJs: null,
    selected: 1,
    isMe: false,
    screenWidth: 300,
    page: 1,
    requestNextPage: false,
    videoList: [],
    videoOne: [],
    showComments: false,
    showVideoOne: false,
    userMessage: null,
    showSettings: false,
    showModify: 0,
    inputValue: '',
    inputLength: 0,
    oldPassword: '',
    newPassword: '',
    rePassword: '',
    tip: null,
    introduce: '添加个人简介帮助别人了解你哦',
    requestUrl: '',
    requestName: '',
    showFaceView: false,
  },
  ready: function () {
    let that = this;
    let appJs = getApp();
    that.setData({
      requestUrl: appJs.serverUrl + '/faceRegister?userId=' + appJs.getGlobalUserInfo().id,
      requestName: 'multipartFile',
    })
    var screenWidth = wx.getSystemInfoSync().screenWidth;
    let videoUserId = that.properties.videoUserId;
    let userId = appJs.getGlobalUserInfo().id;
    let isMe = that.data.isMe;
    let queryUserId = '';
    let queryFanId = '';
    if (videoUserId == '' || videoUserId == userId){
      isMe = true;
      queryUserId = userId;
    } else {
      queryUserId = videoUserId;
      queryFanId = userId;
    }
    that.setData({ screenWidth, appJs, isMe, });
    utils.getUserMessage({ queryUserId, queryFanId }).then((result) => {
      that.setData({
        userMessage: result
      })
    }).catch(() => {
      // 调用返回函数
    })
    if(isMe) {
      queryUserId = userId;
    } else {
      queryUserId = videoUserId;
    }
    utils.getMyVideoList(queryUserId, 1).then((result)=>{
      that.setData({
        videoList: result.list,
        navigateLastPage: result.navigateLastPage
      })
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {
    choose(e){
      let iconId =+ e.currentTarget.dataset.iconid;
      let userId = this.data.userMessage.id;
      let page = this.data.page;
      let that = this;
      this.setData({
        selected: iconId,
        page: 1,
      })
      this.requestVideoList(true, iconId, page, userId);
    },
    closeEdit(){
      this.setData({
        showSettings: false,
      })
    },
    editData(){
      // 编辑 展示
      this.setData({
        showSettings: true,
      })
    },
    followMe(){
      //关注
      let that = this;
      utils.followMe({ isSelectedFollow: this.data.userMessage.isFollow, userId: this.properties.videoUserId }).then((result)=>{
        that.setData({
          'userMessage.isFollow': !that.data.userMessage.isFollow,
        })
        if (that.data.userMessage.isFollow){
          that.setData({
            'userMessage.fansCounts': that.data.userMessage.fansCounts + 1
          })
        } else {
          that.setData({
            'userMessage.fansCounts': that.data.userMessage.fansCounts - 1
          })
        }
      })
    },
    selectedVideo(e){
      let that = this;
      let videoId = e.currentTarget.dataset.videoid;
      utils.getAllVideoList(1, 0, '', '', videoId).then(res => {
        let videoList = res.list;
        videoList[0].video_is_player = true;
        that.setData({
          videoOne: videoList,
          showVideoOne: true,
        })
      })
    },
    closeVideo(){
      let message = { videomessage: this.data.videoOne[0].videoMessage}
      this.triggerEvent('callbackfunc', { callBackId: 1, message: message.videomessage });
      this.setData({
        videoOne: [],
        showVideoOne: false,
      })
    },
    nextDataPage(){
      let that = this;
      if(this.data.requestNextPage){
        return false;
      }
      if (this.data.page == this.data.navigateLastPage){
        wx.showToast({
          title: '没有啦..',
          icon: 'none'
        })
        return false;
      }
      that.setData({
        requestNextPage: true,
        page: this.data.page + 1
      })
      let iconId = this.data.selected;
      let page = this.data.page;
      let userId = this.data.userMessage.id;
      this.requestVideoList(false, iconId, page, userId);
    },
    requestVideoList(isOneRequest, iconId, page, userId){
      let that = this;
      wx.showLoading({
        title: '加载中...',
      })
      if (iconId == 1) {
        // 作品
        utils.getMyVideoList(userId, page).then((result) => {
          wx.hideLoading();
          let videoList = result.list;
          if (!isOneRequest) {
            videoList = this.data.videoList.concat(videoList)
          }
          that.setData({
            videoList,
            navigateLastPage: result.navigateLastPage,
            requestNextPage: false,
          })
        })
      } else if (iconId == 2) {
        // 关注
        utils.getMyFollowList(userId, page).then((result) => {
          wx.hideLoading();
          let videoList = result.list;
          if (!isOneRequest) {
            videoList = this.data.videoList.concat(videoList)
          }
          that.setData({
            videoList,
            navigateLastPage: result.navigateLastPage,
            requestNextPage: false,
          })
        })
      } else if (iconId == 3) {
        // 喜欢
        utils.getMyLikesList(userId, page).then((result) => {
          wx.hideLoading();
          let videoList = result.list;
          if (!isOneRequest) {
            videoList = this.data.videoList.concat(videoList)
          }
          that.setData({
            videoList,
            navigateLastPage: result.navigateLastPage,
            requestNextPage: false,
          })
        })
      }
    },
    // 更换头像
    modifyFace() {
      let that = this;
      // 选择照片
      wx.chooseImage({
        count: 1,
        sizeType: ['compressed'],
        sourceType: ['album'],
        success: function (res) {
          let tempImagePath = res.tempFilePaths[0]
          wx.showLoading({
            title: '上传中...',
          })
          wx.uploadFile({
            url: that.data.appJs.serverUrl + '/user/uploadFace?userId=' + that.data.appJs.getGlobalUserInfo().id,
            filePath: tempImagePath,
            name: 'file',
            success: res => {
              let data = res.data;
              if (data.status === 200) {
                wx.hideLoading();
                that.setData({
                  'userMessage.faceImage': data.data
                })
                wx.showToast({
                  title: '修改成功',
                })
              } else {
                wx.hideLoading();
                wx.showToast({
                  title: data.msg,
                  icon: 'none'
                })
              }
            }
          })
        },
      })
    },
    modify(e){
      let that = this;
      let viewId = e.currentTarget.dataset.viewId
      if (viewId == 1){
        // 修改昵称
        let length = this.data.userMessage.nickname.length;
        let inputValue = this.data.userMessage.nickname;
        this.setData({
          showModify: 1,
          inputLength: length,
          inputValue
        })
      } else if (viewId == 2) {
        // 修改个人简介
        let inputValue = this.data.userMessage.introduce;
        this.setData({
          showModify: 2,
          inputValue
        })
      } else if (viewId == 3) {
        //修改密码
        this.setData({
          showModify: 3
        })
      } else if (viewId == 4) {
        wx.showActionSheet({
          itemList: ['开启微信快捷登录', '关闭微信快捷登录'],
          success: res=>{
            if(res.tapIndex == 0){
              // 微信授权
              wx.showLoading({
                title: '授权中...',
              })
              wx.login({
                success: function (res) {
                  let code = res.code;
                  if (code) {
                    wx.getUserInfo({
                      success: res=>{
                        wx.request({
                          url: that.data.appJs.serverUrl + '/bindWxLogin?userId=' + that.data.appJs.getGlobalUserInfo().id,
                          method: 'POST',
                          data: {
                            encryptedData: res.encryptedData,
                            iv: res.iv,
                            code: code
                          },
                          success: res => {
                            let data = res.data;
                            wx.hideLoading();
                            if (data.status === 200) {
                              wx.showToast({
                                title: '绑定成功',
                                icon: 'none'
                              })
                            } else {
                              wx.showToast({
                                title: data.msg,
                                icon: 'none'
                              })
                            }
                          },
                          fail: () => {
                            wx.showToast({
                              title: '绑定失败',
                              icon: 'none'
                            })
                          }
                        })
                      },
                      fail: () => {
                        wx.hideLoading();
                        wx.showToast({
                          title: '获取信息失败!',
                          icon: 'none'
                        })
                      }
                    })
                  }
                },
                fail: () => {
                  wx.hideLoading();
                  wx.showToast({
                    title: '获取信息失败!',
                    icon: 'none'
                  })
                }
              })
            } else if (res.tapIndex == 1) {
              // 取消微信授权
              wx.showLoading({
                title: '取消中...',
              })
              wx.request({
                url: that.data.appJs.serverUrl + '/deleteBindWeiXin?userId=' + that.data.appJs.getGlobalUserInfo().id,
                method: 'POST',
                success: res=>{
                  let data = res.data
                  wx.hideLoading();
                  if (data.status === 200) {
                    wx.showToast({
                      title: '取消绑定成功',
                      icon: 'none'
                    })
                  } else {
                    wx.showToast({
                      title: data.msg,
                      icon: 'none'
                    })
                  }
                },
                fail: () => {
                  wx.showToast({
                    title: '取消绑定失败',
                    icon: 'none'
                  })
                }
              })
            }
          }
        })
      } else if (viewId == 5) {
        wx.showActionSheet({
          itemList: ['开启人脸快捷登录', '关闭人脸快捷登录'],
          success: res => {
            if (res.tapIndex == 0) {
              // 人脸登录授权
              wx.showLoading({
                title: '授权中...',
              })
              wx.request({
                url: that.data.appJs.serverUrl + '/openFaceLogin?userId=' + that.data.appJs.getGlobalUserInfo().id,
                method: 'POST',
                success: res => {
                  let data = res.data;
                  wx.hideLoading();
                  if (data.status === 200) {
                    wx.showToast({
                      title: '授权成功',
                      icon: 'none'
                    })
                  } else {
                    wx.showToast({
                      title: data.msg,
                      icon: 'none'
                    })
                  }
                },
                fail: () => {
                  wx.showToast({
                    title: '授权失败',
                    icon: 'none'
                  })
                }
              })
            } else if (res.tapIndex == 1) {
              // 取消人脸授权
              wx.showLoading({
                title: '取消中...',
              })
              wx.request({
                url: that.data.appJs.serverUrl + '/closeFaceLogin?userId=' + that.data.appJs.getGlobalUserInfo().id,
                method: 'POST',
                success: res => {
                  let data = res.data;
                  wx.hideLoading();
                  if (data.status === 200) {
                    wx.showToast({
                      title: '取消授权成功',
                      icon: 'none'
                    })
                  } else {
                    wx.showToast({
                      title: data.msg,
                      icon: 'none'
                    })
                  }
                },
                fail: () => {
                  wx.showToast({
                    title: '取消授权失败',
                    icon: 'none'
                  })
                }
              })
            }
          }
        })
      } else if (viewId == 6) {
        that.setData({
          showFaceView: true
        })
      } else if (viewId == 7) {
        wx.showModal({
          title: '退出登录',
          content: '是否要退出登录',
          success: res=>{
            if (res.confirm) {
              wx.showLoading({
                title: '注销中...',
              })
              wx.request({
                url: that.data.appJs.serverUrl + '/loginOut?userId=' + that.data.appJs.getGlobalUserInfo().id,
                method: 'POST',
                success: res=>{
                  wx.removeStorageSync("userInfo")
                  wx.redirectTo({
                    url: '/pages/loginRegister/loginRegister',
                  })
                },
                fail: () => {
                  wx.removeStorageSync("userInfo")
                  wx.redirectTo({
                    url: '/pages/loginRegister/loginRegister',
                  })
                }
              })
            }
          }
        })
      }
    },
    cancenModify(){
      this.setData({
        showModify: 0,
        inputValue: '',
        inputLength: 0,
      })
    },
    inputMessage(e) {
      let that = this;
      let type = e.currentTarget.dataset.inputType;
      if (type == 'nickname') {
        // 修改昵称
        if (e.detail.cursor > 20) {
          return;
        }
        this.setData({
          inputLength: e.detail.cursor,
          inputValue: e.detail.value,
        })
        if (e.detail.cursor == 0) {
          that.setData({
            'tip.nickname': '昵称不能为空',
          })
          return;
        } else {
          that.setData({
            'tip.nickname': '',
          })
        }
      } else if (type == 'introduce') {
        // 修改个人介绍
        this.setData({
          inputValue: e.detail.value,
        })
      } else if (type == 'oldPassword') {
        // 修改老密码
        if (e.detail.cursor == 0) {
          that.setData({
            'tip.oldPassword': '密码不能为空',
          })
        } else {
          that.setData({
            'tip.oldPassword': '',
          })
        }
        let oldPassword = e.detail.value;
        this.setData({
          oldPassword: oldPassword,
        })
      } else if (type == 'newPassword') {
        // 修改新密码
        let newPassword = e.detail.value;
        let oldPassword = this.data.oldPassword;
        if (newPassword == oldPassword) {
          that.setData({
            'tip.newPassword': '与原始密码相同',
          })
        }
        if (e.detail.cursor == 0) {
          that.setData({
            'tip.newPassword': '密码不能为空',
          })
        }
        if (newPassword != oldPassword && e.detail.cursor != 0){
          that.setData({
            'tip.newPassword': '',
          })
        }
        this.setData({
          newPassword: newPassword,
        })
      } else if (type == 'rePassword') {
        // 修改确认密码
        let rePassword = e.detail.value;
        let newPassword = this.data.newPassword;
        if (newPassword != rePassword) {
          that.setData({
            'tip.rePassword': '两次输入的密码不一致',
          })
        }
        if (e.detail.cursor == 0) {
          that.setData({
            'tip.rePassword': '密码不能为空',
          })
        }
        if (newPassword == rePassword && e.detail.cursor != 0) {
          that.setData({
            'tip.rePassword': '',
          })
        }
        this.setData({
          rePassword: rePassword,
        })
      }
    },
    saveNickname(){
      let that = this;
      let nickname = this.data.inputValue;
      if (nickname.length == 0) {
        that.setData({
          'tip.nickname': '昵称不能为空',
        })
        return false;
      }
      that.setData({
        'userMessage.nickname': nickname,
        showModify: 0,
        inputValue: '',
        inputLength: 0,
      })
      wx.showLoading({
        title: '提交中...',
      })
      wx.request({
        url: that.data.appJs.serverUrl + '/user/changeUser',
        method: 'POST',
        data: {
          id: that.data.appJs.getGlobalUserInfo().id,
          nickname,
        },
        success: res=>{
          let data = res.data;
          if (data.status === 200){
            wx.hideLoading();
            wx.showToast({
              title: '修改成功',
              icon: 'none'
            })
          } else {
            wx.hideLoading();
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        }
      })
    },
    saveIntroduce(){
      let that = this;
      let introduce = this.data.inputValue;
      that.setData({
        'userMessage.introduce': introduce,
        showModify: 0,
        inputValue: '',
        inputLength: 0,
      })
      wx.showLoading({
        title: '提交中...',
      })
      wx.request({
        url: that.data.appJs.serverUrl + '/user/changeUser',
        method: 'POST',
        data: {
          id: that.data.appJs.getGlobalUserInfo().id,
          introduce,
        },
        success: res => {
          let data = res.data;
          if (data.status === 200) {
            wx.hideLoading();
            wx.showToast({
              title: '修改成功',
              icon: 'none'
            })
          } else {
            wx.hideLoading();
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        }
      })
    },
    savePassword() {
      let that = this;
      let oldPassword = this.data.oldPassword;
      let newPassword = this.data.newPassword;
      let rePassword = this.data.rePassword;
      if (oldPassword.length == 0 || newPassword.length == 0 || rePassword.length == 0 
        || newPassword != rePassword || newPassword == oldPassword) {
        return false;
      }
      that.setData({
        oldPassword: '',
        newPassword: '',
        rePassword: '',
        showModify: 0,
        inputValue: '',
        inputLength: 0,
      })
      wx.showLoading({
        title: '修改中...',
      })
      wx.request({
        url: that.data.appJs.serverUrl + '/user/changePassword',
        method: 'POST',
        data: {
          userId: that.data.appJs.getGlobalUserInfo().id,
          oldPassword,
          newPassword
        },
        success: res=>{
          let data = res.data;
          if (data.status === 200) {
            wx.hideLoading();
            wx.showToast({
              title: '修改成功',
              icon: 'none'
            })
          } else {
            wx.hideLoading();
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        }
      })
    },
    requestSuccess(res) {
      wx.showToast({
        title: '注册人脸成功',
      })
      this.setData({
        showFaceView: false
      })
    },
    requestFail() {
    },
    closeFaceRequest() {
      this.setData({
        showFaceView: false
      })
    },
    //点击左侧按钮
    menuTap(e) {
      const { buttontype, buttonname, videomessage, videopath, coverpath } = e.detail;
      switch (buttontype) {
        case "1":
          wx.redirectTo({
            url: '../search/search'
          })
          break;
        case "2":
          let videoUserId = videomessage.userId;
          let pageUserId = this.data.userMessage.id;
          if (videoUserId != pageUserId) {
            this.triggerEvent('callbackfunc', { callBackId: 1, message: videomessage });
          }
          break;
        case "3":
          utils.followMe(videomessage)
          break;
        case "4":
          utils.likeVideoOrNot(videomessage)
          break;
        case "5":
          this.openOrCloseComment()
          break;
        case "6":
          utils.shareMe(videomessage, videopath)
          break;
      }
    },
    addOneCommentCount() {
      let video = this.data.videoOne;
      video[0].videoMessage.commentCount = video[0].videoMessage.commentCount+ 1;
      this.setData({
        videoOne: video
      })
    },
    openOrCloseComment() {
      this.setData({
        showComments: !this.data.showComments
      })
    }
  },
})
