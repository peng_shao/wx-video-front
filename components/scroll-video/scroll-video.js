// components/scroll-video.js
Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  /**
   * 组件的属性列表
   */
  properties: {
    //父组件传入的视频列表
    videoList:{
      type: Array,
      value: [],
      observer: function(newVal){
        this.setData({
          videoSize: newVal.length
        });
      }
    },
    //视频或者直播适配页面方式
    fitType:{
      type: String,
      value: 'contain',
    },
    //滑动距离的设置 超过该距离回出现页面下滑或者上滑的情况
    thresholdValue:{
      type: Number,
      value: 100,
      observer: function (newVal, oldVal) {
      }
    },
    //播放器类型
    playerType:{
      type: String,
      value: 'video',
      observer: function (newVal, oldVal) {
      }
    }
    
  },

  /**
   * 组件的初始数据
   */
  data: {
    startX: 0,//开始x点
    startY: 0,//开始y点
    pageIndex: 0, //当前页面id
    screenHeight:0,//获取当前屏幕高度
    screenWidth:0,
    scrollAnimate:0,
    videoidx:0,//保存切换下标
    videoSize:0,//视频列表的长度
    play: true,
  },
  ready:function(){
   this.animation = wx.createAnimation({
     duration:600,
     timingFunction:'linear',
   });
    let that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          screenHeight: res.windowHeight,
          screenWidth: res.windowWidth
        })
      }
    })
    this.setData({
      videoSize: this.properties.videoList.length
    });
  },
  /**
   * 组件的方法列表
   */
  methods: {
    buttonhandle:function(e){
      const { buttontype, buttonname, videomessage, videopath, coverpath } = e.detail;
      this.triggerEvent('menuTap', { buttontype, buttonname, videomessage, videopath, coverpath });
    },
    // 开始停止播放
    playerBtn: function (e) {
      // this.triggerEvent('player', { play: this.data.play, videoId: e.target.id});
      let videoContext = wx.createVideoContext("video-play")
      if (this.data.play) {
        videoContext.pause()
        this.setData({
          play: false
        })
      } else {
        videoContext.play()
        this.setData({
          play: true
        })
      }
    },
    onTouchStart: function (e) {
      const { pageY } = e.changedTouches[0]; //记录手指位置
      this.setData({
        startY: pageY,
      });
    },
    onTouchEnd: function (e) {
      let videoidx = parseInt(e.currentTarget.dataset.videoidx);
      let thresholdValue = this.properties.thresholdValue;
      let changeY = e.changedTouches[0].pageY - this.data.startY;
      if (changeY > 0) {
        // 滑动了
        if (changeY >= thresholdValue) {
          // 第一个视频
          if (videoidx === 0){
            this.triggerEvent('swipeToStart', {
              oldindex: 0,
              newindex: videoidx,
              playerType: this.properties.playerType
            });
            return false;
          }
          // 设置滑动高度
          let top_height = -((videoidx - 1) * this.data.screenHeight);
          this.triggerEvent('swipeDown',{
            newindex: videoidx-1
          });
          this.animation.translateY(top_height).step();
          this.setData({
            scrollAnimate: this.animation.export(),
            videoidx: videoidx - 1,
          });

        } else {
          this.playerBtn(e)
        }
      } else if(changeY < 0) { //向下滑动
        let abschangeY = Math.abs(changeY);
        // 滑动了指定距离切换视频
        if (abschangeY >= thresholdValue) {
          // 最后1个视频
          if (videoidx === this.data.videoSize -1) {
            this.triggerEvent('swipeToEnd', {
              oldindex: videoidx,
              newindex: videoidx+1,
              playerType: this.properties.playerType
            });
            return false;
          }
          // 向下移动距离
          let btm_height = -((videoidx + 1) * this.data.screenHeight);
          this.triggerEvent('swipeUpper', {
            newindex: videoidx + 1
          });
          this.animation.translateY(btm_height).step();
          this.setData({
            scrollAnimate:this.animation.export(),
            videoidx: videoidx + 1,
          });
        } else {
          this.playerBtn(e)
        }
      }
    },
  }
})
