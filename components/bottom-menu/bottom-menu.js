// components/bottom-menu/bottom-menu.js
Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  /**
   * 组件的属性列表
   */
  properties: {
    color: {
      type: String,
      value: ''
    },
    show: {
      type: Boolean,
      value: true
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    toolBarButton: function (e) {
      const { buttontype, buttonname} = e.currentTarget.dataset;
      this.triggerEvent('menubottom', { buttontype, buttonname});
    }
  }
})
