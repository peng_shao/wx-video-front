// components/video-button-bar.js
Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  /**
   * 组件的属性列表
   */
  properties: {
    videoMessage: {
      type: Object,
      value: {}
    },
    videoPath: {
      type: String,
      value: ''
    },
    coverPath: {
      type: String,
      value: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isMe: false
  },
  ready: function () {
    let isMe = this.properties.videoMessage.userId == getApp().getGlobalUserInfo().id
    this.setData({
      isMe
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {
    toolBarButton: function (e) {
      const { buttontype, buttonname, videomessage} = e.currentTarget.dataset;
      switch (buttontype) {
        case "3":
          if (this.data.videoMessage.isSelectedFollow === 0){
            this.setData({
              'videoMessage.isSelectedFollow': 1
            })
          }else{
            this.setData({
              'videoMessage.isSelectedFollow': 0
            })
          }
          break;
        case "4":
          let likeCount = this.data.videoMessage.likeCount;
          if (this.data.videoMessage.isFav === 0) {
            this.setData({
              'videoMessage.isFav': 1,
              'videoMessage.likeCount': likeCount + 1
            })
          } else {
            this.setData({
              'videoMessage.isFav': 0,
              'videoMessage.likeCount': likeCount - 1
            })
          }
          break;
      }
      this.triggerEvent('buttonhandle', 
        { 
          buttontype, buttonname, videomessage, 
          'videopath': this.data.videoPath,
          'coverpath': this.data.coverPath
        }
      );
    }
  }
})
