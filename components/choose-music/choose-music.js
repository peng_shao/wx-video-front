// components/choose-music/choose-music.js
const myaudio = wx.createInnerAudioContext();
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    screenHeight: 500,
    screenWidth: 200,
    appJs: null,
    //是否播放
    isPlay: false,
    //正在播放的id
    playId: null,
    input: '',
    showTime: '',
    audioList: []
  },
  ready: function () {
    let that = this;
    myaudio.onEnded(()=>{
      that.setData({
        isPlay: false,
      })
    })
    this.setData({
      appJs: getApp()
    })
    let { screenHeight, screenWidth } = wx.getSystemInfoSync()
    that.setData({
      screenHeight,
      screenWidth
    })
    this.onLoadMusic()
  },
  /**
   * 组件的方法列表
   */
  methods: {
    onLoadMusic(parms){
      let that = this;
      wx.request({
        url: that.data.appJs.serverUrl + '/bgm/queryBgmList',
        method: "POST",
        header: {
          'content-type': 'application/json', // 默认值
        },
        success: function (res) {
          let data = res.data;
          if (data.status == '200' || data.status == 200) {
            that.setData({
              audioList: data.data
            });
          } else if (data.status == '502' || data.status == 502) {
            wx.showToast({
              title: data.msg,
              duration: 2000,
              icon: "none",
            });
          }
        }
      }) 
    },
    searchInput(e) {
      this.setData({
        input: e.detail.value
      })
    },
    searchMusic() {
      let that = this;
      wx.request({
        url: that.data.appJs.serverUrl + '/bgm/searchBgm?bgmName=' + that.data.input,
        method: "POST",
        header: {
          'content-type': 'application/json', // 默认值
        },
        success: function (res) {
          let data = res.data;
          if (data.status == 200) {
            that.setData({
              audioList: data.data,
              input: ''
            });
          } else {
            wx.showToast({
              title: data.msg,
              duration: 2000,
              icon: "none",
            });
          }
        }
      })
    },
    close(){
      this.stop();
      this.triggerEvent('closeMusic', {});
    },
    onTopMusic(parms){
      let audioId = parms.currentTarget.dataset.index;
      if(this.data.isPlay && this.data.playId == audioId){
        this.stop();
      } else {
        this.play(parms);
      }
    },
    // 播放音乐
    play(parms){
      let that = this;
      let audioId = parms.currentTarget.dataset.index;
      myaudio.src = this.data.audioList[audioId].path;
      myaudio.play();
      myaudio.onError((e) => {
        let errCode = e.errCode;
        if (errCode == 10001) {
          wx.showToast({
            title: '系统错误，无法播放！',
            icon: 'none'
          })
        } else if (errCode == 10002) {
          wx.showToast({
            title: '网络错误，无法播放！',
            icon: 'none'
          })
        } else if (errCode == 10003) {
          wx.showToast({
            title: '文件错误，无法播放！',
            icon: 'none'
          })
        } else if (errCode == 10004) {
          wx.showToast({
            title: '格式错误，无法播放！',
            icon: 'none'
          })
        } else if (errCode == -1) {
          wx.showToast({
            title: '未知错误，无法播放！',
            icon: 'none'
          })
        }
        that.stop();
      });
      this.setData({
        isPlay: true,
        playId: audioId
      })
    },
    // 停止音乐
    stop(){
      myaudio.stop();
      this.setData({
        isPlay: false,
      })
    },
    // 选择音乐
    choose(parms){
      let audioId = parms.currentTarget.dataset.index;
      let audio = this.data.audioList[audioId];
      this.stop()
      this.triggerEvent('playMusic', { id: audio.id, audioPath: audio.path, audioName: audio.name + '---' + audio.author});
      this.close();
    }
  }
})
