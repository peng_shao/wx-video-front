// components/face-verify/face-verify.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    requestUrl: {
      type: String,
      value: ''
    },
    requestName: {
      type: String,
      value: ''
    }

  },

  /**
   * 组件的初始数据
   */
  data: {
    errMsg: '',
    coverImage: '',
    src: '',
    height: '',
  },
  ready: function() {
    let that = this;
    wx.getSystemInfo({
      success: function (res) {
        // 获取可使用窗口宽度
        let clientHeight = res.windowHeight;
        // 获取可使用窗口高度
        let clientWidth = res.windowWidth;
        // 算出比例
        let ratio = 750 / clientWidth;
        // 算出高度(单位rpx)
        let height = clientHeight * ratio;
        // 设置高度
        that.setData({
          height: height,
          coverImage: getApp().faceLoginImg,
        });
      }
    });
    setTimeout(function() {
      that.login()
    }, 2000);
  },
  /**
   * 组件的方法列表
   */
  methods: {

    // 登录
    login(e) {
      this.takePhoto();
    },

    // 发送请求
    sendRequest() {
      let that = this;
      wx.uploadFile({
        url: that.properties.requestUrl, //app.serverUrl + '/faceLogin',
        filePath: that.data.src,
        // name: 'multipartFile',
        name: that.properties.requestName,
        success: function (res) {
          var data = JSON.parse(res.data);
          if (data.status == 200 || data.status == '200') {
            // wx.showToast({
            //   title: '登录成功',
            //   icon: 'success',
            //   duration: 2000
            // });
            // app.userInfo = res.data.data;
            // fixme 修改原有的全局对象为本地缓存
            // app.setGlobalUserInfo(data.data);
            // 调用成功函数并把返回值
            that.triggerEvent('requestSuccess', { data });
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none',
              duration: 2000
            });
            that.triggerEvent('requestFail', { data });
            that.setData({
              errMsg: data.msg
            })
          }
        },
        fail: function () {
          wx.showToast({
            title: '登录错误,请点击屏幕重新登录',
            icon: 'none',
            duration: 2000
          });
          that.setData({
            errMsg: '登录错误,请点击屏幕重新登录'
          })
        },
        complete: function () {
          wx.hideLoading();
        }
      })
    },

    // 重新登录
    relogin: function (res) {
      let that = this;
      setTimeout(function () {
        that.login()
      }, 2000);
      that.setData({
        errMsg: ''
      })
    },

    // 拍照
    takePhoto() {
      let that = this;
      const ctx = wx.createCameraContext()
      ctx.takePhoto({
        quality: 'high',
        success: (res) => {
          that.setData({
            src: res.tempImagePath
          })
          that.sendRequest();
        },
        fail: function (res) {
          wx.showToast({
            title: '拍照错误',
            icon: 'none',
            duration: 2000
          });
          that.setData({
            errMsg: '拍照错误'
          })
        }
      })
    },
    error(e) {
      wx.showToast({
        title: '请允许小程序使用摄像头',
        icon: 'none',
        duration: 2000
      });
      that.setData({
        errMsg: '请允许小程序使用摄像头'
      })
    },
    close(){
      this.triggerEvent('closeFaceRequest', {})
    }
  }
})
