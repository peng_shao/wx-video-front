// components/report/report.js
const app = getApp()

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    videoId: {
      type: String,
      value: ''
    },
    publishUserId: {
      type: String,
      value: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    reasonType: "请选择原因",
    reportReasonArray: app.reportReasonArray,
  },
  /**
   * 组件的方法列表
   */
  methods: {
    changeMe: function (e) {
      var me = this;
      var index = e.detail.value;
      var reasonType = app.reportReasonArray[index];
      me.setData({
        reasonType: reasonType
      });
    },

    submitReport: function (e) {
      var me = this;
      var reasonIndex = e.detail.value.reasonIndex;
      var reasonContent = e.detail.value.reasonContent;
      var user = app.getGlobalUserInfo();
      var currentUserId = user.id;

      if (!reasonIndex) {
        wx.showToast({
          title: '选择举报理由',
          icon: "none"
        })
        return;
      }
      var serverUrl = app.serverUrl;
      wx.request({
        url: serverUrl + '/user/reportUser',
        method: 'POST',
        data: {
          dealUserId: me. properties.publishUserId,
          dealVideoId: me.properties.videoId,
          title: app.reportReasonArray[reasonIndex],
          content: reasonContent,
          userId: currentUserId
        },
        header: {
          'content-type': 'application/json', // 默认值
          'headerUserId': user.id,
          'headerUserToken': user.userToken
        },
        success: function (res) {
          wx.showToast({
            title: "举报成功...有你平台变得更美好...",
            duration: 2000,
            icon: 'none',
          })
          // wx.navigateBack();
          me.triggerEvent('reportSuccess');
        }
      })
    }
  }
})
