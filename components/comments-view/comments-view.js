// components/comments-view/comments-view.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    videoId: {
      type: Int32Array,
      value: 0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    appJs: null,
    contentValue: '',
    commentsList: [],
    commentsPage: 1,
    commentsTotalPage: 1,
  },
  ready: function () {
    this.setData({
      appJs: getApp()
    })
    this.getCommentsList(1);
    this.leaveComment();
  },
  /**
   * 组件的方法列表
   */
  methods: {
    leaveComment: function () {
      this.setData({
        placeholder: "说点什么...",
        commentFocus: true,
        replyFatherCommentId: "",
        replyToUserId: "",
      });
    },
    replyFocus: function (e) {
      var fatherCommentId = e.currentTarget.dataset.fathercommentid;
      var toUserId = e.currentTarget.dataset.touserid;
      var toNickname = e.currentTarget.dataset.tonickname;
      this.setData({
        placeholder: "回复  " + toNickname,
        replyFatherCommentId: fatherCommentId,
        replyToUserId: toUserId,
        commentFocus: true
      });
    },
    saveComment: function (e) {
      let that = this;
      let content = e.detail.value;
      let user = that.data.appJs.getGlobalUserInfo();
      // 获取评论回复的fatherCommentId和toUserId
      let fatherCommentId = e.currentTarget.dataset.replyfathercommentid;
      let toUserId = e.currentTarget.dataset.replytouserid;

      wx.showLoading({
        title: '请稍后...',
      })
      wx.request({
        url: that.data.appJs.serverUrl + '/video/saveComment',
        method: 'POST',
        header: {
          'content-type': 'application/json', // 默认值
          'headerUserId': user.id,
          'headerUserToken': user.userToken
        },
        data: {
          fatherCommentId: fatherCommentId,
          toUserId: toUserId,
          fromUserId: user.id,
          videoId: that.properties.videoId,
          comment: content
        },
        success: function (res) {
          wx.hideLoading();
          that.setData({
            contentValue: "",
            commentsList: []
          });
          that.getCommentsList(1);
          // 调用评论加1
          that.triggerEvent('send', {});
        }
      })
      this.leaveComment()
    },
    getCommentsList: function (page) {
      var that = this;
      var videoId = that.properties.videoId;
      wx.request({
        url: that.data.appJs.serverUrl + '/video/getVideoComment?videoId=' + videoId + '&page=' + page,
        method: 'POST',
        success: function (res) {
          let data = res.data;
          var commentsList = that.data.commentsList;
          var newCommentsList = data.data.list;
          that.setData({
            commentsList: commentsList.concat(newCommentsList),
            commentsPage: page,
            commentsTotalPage: data.data.pages,
          })
        }
      })
    },
    closeComment(){
      this.triggerEvent('close', {});
    }
  }
})
