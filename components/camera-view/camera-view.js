// components/camera-view/camera-view.js
const myaudio = wx.createInnerAudioContext();
const utils = require('../../utils/utils.js');

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    screenHeight: {
      type: Int32Array,
      value: 0
    },
    screenWidth: {
      type: Int32Array,
      value: 0
    },
    backPage: {
      type: Int32Array,
      value: 1
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    // 是否为后置摄像头
    device: true,
    // 拍照的临时图片地址
    tempImagePath: "",
    // 录制视频的临时缩略图地址
    tempThumbPath: "",
    // 录制视频的临时视频地址
    tempVideoPath: "",
    // 视频/图片高度
    tmpHeight: 0,
    // 视频/图片宽度
    tmpWidth: 0,
    // 是否在拍摄
    showView: 1,
    // 相机信息
    ctx: {},
    // 拍照/录像
    type: "takePhoto",
    // 是否开始录像
    startRecord: false,
    // 录像时间s
    time: 0,
    timeLoop: "",
    // 显示音乐
    showMusic: false,
    // 播放背景音乐
    isPlay: false,
    musicMessage: '选择音乐',
    musicId: 0,
    // 视频描述
    desc: '',
    // 谁可以看
    jurisdictionMessage: '公开',
    jurisdictionCode: 0,
  },
  /**
   * 组件初始化
   */
  ready: function () {
    let { screenHeight, screenWidth} = wx.getSystemInfoSync();
    this.setData({
      ctx: wx.createCameraContext(),
      tmpHeight: screenHeight,
      tmpWidth: screenWidth
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {
    // 拍照方法
    camera() {
      let { ctx, startRecord } = this.data;
      // 拍照
      if (this.data.type == "takePhoto") {
        ctx.takePhoto({
          quality: "normal",
          success: (res) => {
            this.setData({
              tempImagePath: res.tempImagePath,
              showView: 2,
            });
          },
          fail: (e) => {
            wx.showToast({
              title: '拍摄照片失败',
              icon: 'error',
              duration: 2000
            });
          }
        })
      }
      // 录视频
      else if (this.data.type == "startRecord") {
        // 播放背景音乐
        myaudio.play();
        if (!startRecord) {
          this.setData({
            startRecord: true
          });
          // 30秒倒计时
          let t1 = 0;
          let timeLoop = setInterval(() => {
            t1++;
            this.setData({
              time: t1,
            })
            // 最长录制30秒
            if (t1 == 30) {
              clearInterval(timeLoop);
              this.stopRecord(ctx);
            }
          }, 1000);
          this.setData({
            timeLoop
          })
          // 开始录制
          ctx.startRecord({
            success: (res) => {},
            fail: (e) => {
              wx.showToast({
                title: '录制视频失败',
                icon: 'error',
                duration: 2000
              });
            }
          })
        }
        else {
          this.stopRecord(ctx);
        }
      }
    },
    // 停止录制
    stopRecord(ctx) {
      // 停止播放背景音乐
      myaudio.stop();
      let that = this;
      clearInterval(this.data.timeLoop);
      ctx.stopRecord({
        success: (res) => {
          that.setData({
            tempThumbPath: res.tempThumbPath,
            tempVideoPath: res.tempVideoPath,
            showView: 2,
            startRecord: false,
          });
        },
        fail: (e) => {
          wx.showToast({
            title: '录制视频失败',
            icon: 'error',
            duration: 2000
          });
        }
      })
    },
    // 播放背景音乐
    playMusic(e){
      myaudio.src = e.detail.audioPath;
      this.setData({
        musicMessage: e.detail.audioName,
        musicId: e.detail.id,
      })
      this.scrolltxt()
    },
    // 选择音乐
    chooseMusic(){
      let showMusic = this.data.showMusic;
      if(showMusic){
        this.setData({
          showMusic: false,
          showView: 1,
        })
      } else {
        this.setData({
          showMusic: true,
          showView: 4,
        })
      }
    },
    closeMusic(){
      this.setData({
        showMusic: false,
        showView: 1,
      })
    },
    // 重新拍摄
    remake(){
      this.setData({
        showView: 1,
        // 拍照的临时图片地址
        tempImagePath: "",
        // 录制视频的临时缩略图地址
        tempThumbPath: "",
        // 录制视频的临时视频地址
        tempVideoPath: "",
        desc: '',
        musicId: 0,
        time: 0,
        musicMessage: '选择音乐',
      })
      myaudio.src = '';
    },
    // 下一步填写信息
    next(){
      this.setData({
        showView: 3,
      })
    },
    back(){
      this.setData({
        showView: 2,
      })
    },
    // 点击关闭返回
    close() {
      let that = this;
      wx.showModal({
        title: '',
        content: '是否退出拍摄',
        success(res){
          if(res.confirm){
            that.triggerEvent('backPage', { 'showPrePage': that.data.backPage });
          }
        }
      })
      that.setData({
        musicMessage: '选择音乐'
      })
      myaudio.src = '';
    },
    textareaInput(e){
      this.setData({
        desc: e.detail.value,
      })
    },
    // 选择照片/视频
    choiceAlbum() {
      let that = this;
      wx.showActionSheet({
        itemList: ['选择照片', '选择视频'],
        success(res) {
          if (res.tapIndex == 0) {
            // 选择照片
            wx.chooseImage({
              count: 1,
              sizeType: ['original', 'compressed'],
              sourceType: ['album'],
              success: function (res) {
                that.setData({
                  tempImagePath: res.tempFilePaths[0],
                  type:'takePhoto',
                  showView: 2,
                })
              },
            })
          } else if (res.tapIndex == 1) {
            // 选择视频
            wx.chooseVideo({
              sourceType: ['album'],
              maxDuration: 60,
              success: function (res) {
                that.setData({
                  tempThumbPath: res.thumbTempFilePath,
                  tempVideoPath: res.tempFilePath,
                  type: 'startRecord',
                  showView: 2,
                  time: res.duration,
                  tmpHeight: res.height,
                  tmpWidth: res.width,
                });
              }
            })
          }
        }
      })
    },
    // 切换拍照/录像
    converter(e){
      let { type } = e.target.dataset;
      if (type == "takePhoto") {
        wx.showToast({
          title: '拍照模式',
          icon: 'none',
          duration: 2000
        })
      } else {
        wx.showToast({
          title: '录像模式',
          icon: 'none',
          duration: 2000
        })
      }
      this.setData({
        showView: 1,
        type,
        musicMessage: '选择音乐',
      })
      myaudio.src = '';
    },
    // 切换相机前后置摄像头
    devicePosition() {
      if (this.data.device) {
        wx.showToast({
          title: '开启前置摄像头',
          icon: 'none',
          duration: 2000
        })
      } else {
        wx.showToast({
          title: '开启后置摄像头',
          icon: 'none',
          duration: 2000
        })
      }
      this.setData({
        device: !this.data.device,
      })
    },
    error(e) {
      wx.showToast({
        title: '请允许小程序使用摄像头',
        icon: 'error',
        duration: 2000
      });
    },
    scrolltxt(start) {
      var that = this;
      let musicMessage = that.data.musicMessage+';  ';
      that.setData({
        musicMessage: musicMessage
      })
      var interval = setInterval(function () {
        let text = that.data.musicMessage;
        let firstWord = text[0];
        let otherWord = text.substring(1,text.length)
        that.setData({
          musicMessage: otherWord + firstWord
        })
        console.log(that.data.musicMessage)
      }, 1000);
    },
    // 谁可以看
    jurisdiction(){
      let that = this;
      wx.showActionSheet({
        itemList: ['公开', '仅自己'],
        success: function (res) {
          if (res.tapIndex == 0) {
            that.setData({
              jurisdictionMessage: '公开',
              jurisdictionCode: 0
            })
          } else if (res.tapIndex == 1) {
            that.setData({
              jurisdictionMessage: '仅自己',
              jurisdictionCode: 1
            })
          }
        }
      })
    },
    // 上传
    upload(){
      let that = this;
      let uploadUrl = this.data.tempVideoPath
      if (this.data.type == 'takePhoto'){
        uploadUrl = this.data.tempImagePath
      }
      let parms = {
        tmpVideoUrl: uploadUrl,
        bgmId: this.data.musicId,
        desc: this.data.desc,
        tmpHeight: this.data.tmpHeight,
        tmpWidth: this.data.tmpWidth,
        videoSeconds: this.data.time,
        jurisdictionCode: this.data.jurisdictionCode
      }
      utils.uploadVideo(parms).then((result)=>{
        if (result) {
          that.triggerEvent('backPage', { 'showPrePage': that.data.backPage });
        }
      });
    }
  },                                   
})
