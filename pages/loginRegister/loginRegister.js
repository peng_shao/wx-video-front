// pages/loginRegister/loginRegister.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    src: '../../images/icon1.png',
    submitText: '登录',
    changeText: '注册',
    username: null,
    password: null,
    repassword: null,
    type: true,
    showPassword: false,
    wxEmpower: false,
    tip: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  home(){
    wx.redirectTo({
      url: '/pages/index/index',
    })
  },
  /**
   * 登录/注册切换
   */
  changeType(){
    if(this.data.type){
      //登录
    } else {
      //注册
    }
    this.setData({
      type: !this.data.type,
      src: '../../images/icon1.png',
      username: null,
      password: null,
      repassword: null,
      showPassword: false,
      tip: null,
    })
  },
  showPassword(){
    this.setData({
      showPassword: !this.data.showPassword,
    })
  },
  submit(){
    let that = this;
    if (that.data.tip){
      wx.showToast({
        title: that.data.tip,
        icon: 'none',
      })
      return;
    }
    let {username, password, repassword} = that.data;
    if (!username) {
      wx.showToast({
        title: '请输入账号！',
        icon: 'none',
      })
      return;
    } else if (!password) {
      wx.showToast({
        title: '请输入密码！',
        icon: 'none',
      })
      return;
    } else if (!repassword && !that.data.type) {
      wx.showToast({
        title: '请输入确认密码！',
        icon: 'none',
      })
      return;
    } else if (repassword != password && !that.data.type) {
      wx.showToast({
        title: '两次密码不一致！',
        icon: 'none',
      })
      return;
    }
    if (that.data.type) {
      //登录
      wx.showLoading({
        title: '登录中...',
      })
      wx.request({
        url: app.serverUrl + '/login',
        method: 'POST',
        data: {
          username,
          password
        },
        success: res=> {
          let data = res.data
          let status = data.status
          wx.hideLoading();
          if (status == 200) {
            wx.showToast({
              title: '登录成功！',
            })
            app.setGlobalUserInfo(data.data)
            // 跳转
            wx.redirectTo({
              url: '/pages/index/index',
            })
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        },
        fail: ()=> {
          wx.hideLoading();
          wx.showToast({
            title: '登录失败！',
            icon: 'none'
          })
        }
      })
    } else {
      //注册
      wx.showLoading({
        title: '注册中...',
      })
      wx.request({
        url: app.serverUrl + '/register',
        method: 'POST',
        data: {
          username,
          password
        },
        success: res => {
          let data = res.data
          let status = data.status
          wx.hideLoading();
          if (status == 200) {
            wx.showToast({
              title: '注册成功！',
            })
            app.setGlobalUserInfo(data.data)
            // 跳转
            wx.redirectTo({
              url: '/pages/index/index',
            })
          } else {
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        },
        fail: () => {
          wx.hideLoading();
          wx.showToast({
            title: '注册失败！',
            icon: 'none'
          })
        }
      })
    }
  },
  bindGetUserInfo(e){
    if (e.detail.userInfo) {
      //用户按了允许授权按钮
      this.wxLogin()
    } else {
      //用户按了拒绝按钮
      wx.showToast({
        title: '获取信息失败!',
      })
    }
  },
  wxLogin(){
    wx.showLoading({
      title: '登陆中...',
    })
    wx.login({
      success: res => {
        let code = res.code;
        if (code) {
          wx.getUserInfo({
            success: res => {
              wx.request({
                url: app.serverUrl + '/wxLogin',
                method: 'POST',
                data: {
                  encryptedData: res.encryptedData,
                  iv: res.iv,
                  code: code
                },
                success: res=> {
                  let data = res.data;
                  wx.hideLoading();
                  if (data.status == 200){
                    wx.showToast({
                      title: '登录成功！',
                    })
                    app.setGlobalUserInfo(data.data);
                    wx.redirectTo({
                      url: '/pages/index/index',
                    })
                  } else {
                    wx.showToast({
                      title: data.msg,
                      icon: 'none'
                    })
                  }
                }
              })
            },
            fail: () => {
              wx.hideLoading();
              wx.showToast({
                title: '获取信息失败!',
                icon: 'none'
              })
            }
          })
        } else {
          wx.hideLoading();
          wx.showToast({
            title: '获取信息失败!',
            icon: 'none'
          })
        }
      },
      fail: () => {
        wx.hideLoading();
        wx.showToast({
          title: '获取信息失败!',
          icon: 'none'
        })
      }
    })
  },
  forgetPassword(){
    wx.showToast({
      title: '努力开发中...!',
      icon: 'none'
    })
  },
  usernameInput(e){
    this.setData({
      src: '../../images/icon1.png',
      username: e.detail.value
    })
  },
  passwordInput(e){
    this.setData({
      password: e.detail.value
    })
  },
  repasswordInput(e){
    this.setData({
      repassword: e.detail.value
    })
  },
  usernameBlur(e){
    let that = this;
    let username = e.detail.value
    if (!username) {
      this.setData({
        tip: '请输入账号！',
      })
      wx.showToast({
        title: this.data.tip,
        icon: 'none'
      })
    } else {
      this.setData({
        tip: null,
      })
    }
    if (this.data.type) {
      wx.request({
        url: app.serverUrl + '/user/headPortrait?username=' + username,
        method: 'POST',
        success: res=> {
          let data = res.data;
          let status = data.status
          if (status == 200) {
            that.setData({
              src: data.data
            })
          } else {
            that.setData({
              src: '../../images/icon1.png',
            })
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        },
        fail: ()=> {
          that.setData({
            src: '../../images/icon1.png',
          })
        }
      })
    }
  },
  passwordBlur(e) {
    let password = e.detail.value
    if (!password) {
      this.setData({
        tip: '请输入密码！',
      })
      wx.showToast({
        title: this.data.tip,
        icon: 'none'
      })
    } else {
      this.setData({
        tip: null,
      })
    }
  },
  repasswordBlur(e) {
    let repassword = e.detail.value
    if (!repassword) {
      this.setData({
        tip: '请输入确认密码！',
      })
      wx.showToast({
        title: this.data.tip,
        icon: 'none'
      })
    } else if(repassword != this.data.password){
      this.setData({
        tip: '两次密码不一致!',
      })
      wx.showToast({
        title: this.data.tip,
        icon: 'none'
      })
    } else {
      this.setData({
        tip: null,
      })
    }
  },
  faceLogin(){
    wx.navigateTo({
      url: '/pages/faceLogin/faceLogin',
    })
  }
})