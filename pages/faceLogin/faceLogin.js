const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    requestUrl: '',
    requestName: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      requestUrl: app.serverUrl + '/faceLogin',
      requestName: 'multipartFile'
    })
  },
  requestSuccess: (res)=> {
    let requestResult = res.detail.data;
    app.setGlobalUserInfo(requestResult.data);
    wx.redirectTo({
      url: '/pages/index/index',
    })
  },
  requestFail: (res)=> {
  },
  closeFaceRequest: ()=>{
    wx.redirectTo({
      url: '/pages/loginRegister/loginRegister',
    })
  }
})