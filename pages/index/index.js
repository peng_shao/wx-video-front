//index.js
//获取应用实例
const utils = require('../../utils/utils.js');
const app = getApp()

Page({
  data: {
    //显示页面的id
    showPage:1,
    //上次页面的id
    showPrePage:1,
    totalPage: 1,
    page: 1,
    videoId: 0,
    hasNextPage: false,
    //测试视频列表
    videoList: [],
    playerType:'video',
    fitType:'contain',
    // 显示评论
    showComment: false,
    // 底部菜单颜色
    color: '',
    // 是否显示底部菜单
    isShow: true,
    // 手机屏幕高度
    screenHeight: 300,
    screenWidth: 200
  },
  //修改视频属性 保证只有一个video被创建
  controlVideoPlayer: function (list, index) {
    if(list.length===0){
       return [];
    }else{
       list.forEach((item,i)=>{
         if (index === i){
           item.video_is_player=true;
         }else{
           item.video_is_player=false;
         }
       });
       return list;
    }
  },
  onLoad:function(parm){
    let that = this;
    let { isSaveRecord, searchContent, userId, videoId} = parm;
    if (isSaveRecord == null) { isSaveRecord = 0; }
    if (searchContent == null) { searchContent = ''; }
    if (userId == null) { userId = ''; }
    if (videoId == null) { videoId = ''; }
    var screenHeight = wx.getSystemInfoSync().screenHeight;
    var screenWidth = wx.getSystemInfoSync().screenWidth;
    this.setData({
      screenHeight: screenHeight,
      screenWidth,
    });
    let result = utils.getAllVideoList(this.data.page, isSaveRecord, searchContent, userId, videoId).then(res=>{
      let videolist = res.list;
      if (videolist.length === 0) {
        wx.showToast({
          title: '未找到相关视频',
          duration: 3000,
          icon: 'none'
        })
        that.onLoad({})
      } else {
        that.setData({
          videoId: videolist[0].id,
          videoList: that.controlVideoPlayer(videolist, 0),
          hasNextPage: res.hasNextPage,
          totalPage: res.pages,
        });
        wx.hideLoading();
      }
    });
  },
  //上滑事件
  swipeUpper:function(e){
    const {newindex}=e.detail;
    let videolist = this.controlVideoPlayer(this.data.videoList, newindex);
    this.setData({
      videoId: videolist[newindex].id,
      videoList: videolist
    });
  },
  //下滑事件
  swipeDown:function(e){
    const { newindex } = e.detail;
    let videolist = this.controlVideoPlayer(this.data.videoList, newindex);
    this.setData({
      videoId: videolist[newindex].id,
      videoList: videolist
    });
  },
  //上滑到第一条数据
  swipeToStart: function (e) {
    wx.showLoading({
      title: '正在刷新....',
    })
    this.onLoad({})
  },
  //下滑到最后一条数据
  swipeToEnd(e) {
    let that = this;
    let hasNextPage = this.data.hasNextPage;
    if (hasNextPage) {
      wx.showLoading({
        title: '加载中...',
        icon: 'none'
      })
      utils.getAllVideoList(this.data.page + 1, 0, '', '', '').then(res => {
        wx.hideLoading();
        let list = this.data.videoList.concat(res.list);
        that.setData({
          videoList: list,
          hasNextPage: res.hasNextPage,
          totalPage: res.pages,
          page: this.data.page + 1,
        });
      });
    } else {
      wx.showToast({
        title: '没有了, 按主页刷新',
        icon: 'none'
      })
    }
  },
  //点击左侧按钮
  menuTap:function(e){
    let loginUser = app.getGlobalUserInfo()
    if("" == loginUser){
      wx.redirectTo({
        url: '/pages/loginRegister/loginRegister',
      })
      return;
    }
    const { buttontype, buttonname, videomessage, videopath, coverpath } = e.detail;
    switch (buttontype){
      case "1":
        wx.redirectTo({
          url: '../search/search'
        })
      break;
      case "2":
        this.setData({
          showPage: 3,
          showPrePage: this.data.showPage,
          color: 'black',
          isShow: true,
          videoMessage: videomessage
        })
      break;
      case "3":
        utils.followMe(videomessage)
      break;
      case "4":
        utils.likeVideoOrNot(videomessage)
      break;
      case "5":
        this.openOrCloseComment()
      break;
      case "6":
        utils.shareMe(videomessage, videopath)
      break;
    }
  },
  // 打开或关闭评论
  openOrCloseComment(){
    let loginUser = app.getGlobalUserInfo()
    if ("" == loginUser) {
      wx.redirectTo({
        url: '/pages/loginRegister/loginRegister',
      })
      return;
    }
    if (this.data.showComment) {
      this.setData({
        showComment: false,
      })
    } else {
      this.setData({
        showComment: true,
      })
    }
  },
  // 点击底部导航条
  menubottom: function(e){
    let that = this;
    let { buttontype, buttonname, itemid } = e.detail;
    let showPrePage = that.data.showPage;
    let loginUser = app.getGlobalUserInfo()
    if ("" == loginUser && buttontype != "1") {
      wx.redirectTo({
        url: '/pages/loginRegister/loginRegister',
      })
      return;
    }
    switch (buttontype) {
      case "1":
        that.setData({ showPage: 0 })
        that.setData({
          showPage: 1,
          showPrePage: showPrePage,
          color: '',
          isShow: true,
        })
        that.onLoad({})
      break;
      case "2": 
        that.setData({
          showPage: 2,
          showPrePage: showPrePage,
          color: '',
          isShow: false,
        })
      break;
      case "3":
        // 解决第二次点没反应 可以刷新
        that.setData({ showPage: 0})
        that.setData({
          showPage: 3,
          showPrePage: showPrePage,
          color: 'black',
          isShow: true,
          videoMessage: {}
        })
      break;
    }
  },
  backPage: function (e) {
    let showPrePage = e.detail.showPrePage;
    if (showPrePage === 1){
      this.setData({
        showPage: 1,
        showPrePage: showPrePage,
        color: '',
        isShow: true,
      })
      this.onLoad({})
    } else if (showPrePage === 3){
      this.setData({
        showPage: 3,
        showPrePage: showPrePage,
        color: 'black',
        isShow: true
      })
    }
  },
  player(e){
    let { play, videoId} = e.detail;
    let videoContext = wx.createVideoContext(videoId)
    if (play) {
      //暂停播放
      videoContext.pause()
    } else {
      //开始播放
      videoContext.play()
    }
  },
  addOneCommentCount(){
    let list = this.data.videoList;
    let index = this.data.videoId;
    list.forEach((item, i) => {
      if (item.id === index) {
        item.videoMessage.commentCount = item.videoMessage.commentCount + 1;
      }
    });
    this.setData({
      videoList: list
    })
  },
  callbackfunc(e) {
    let { callBackId, message } = e.detail
    if (callBackId == 1) {
      this.setData({ showPage: 0 })
      this.setData({
        showPage: 3,
        showPrePage: this.data.showPage,
        color: 'black',
        isShow: true,
        videoMessage: message
      })
    }
  },
  /**
   * 分享好友
   */
  onShareAppMessage: function (e) {
    let videoId = 3;
    return {
      title: '时拍短视频分享',
      path: "pages/index/index?videoId=" + videoId,
      success: function (res) {
        console.log('成功', res)
      }
    }
  }
})

