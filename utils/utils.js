const app = getApp()
/**
 * 获取视频信息
 */
function getAllVideoList(page, isSaveRecord, searchContent, userId, videoId) {
  var serverUrl = app.serverUrl;
  var user = app.getGlobalUserInfo();
  return new Promise((resolve, reject) => {
    wx.request({
      url: serverUrl + '/video/showAll?page=' + page +
        "&loginUserId=" + user.id + 
        "&isSaveRecord=" + isSaveRecord,
      method: "POST",
      data: {
        id: videoId,
        videoDesc: searchContent,
        userId: userId,
      },
      success: res => {
        resolve(res.data.data);
      }
    })
  });
}
/**
 * 关注/取消关注
 */
function followMe(parms) {
  var user = app.getGlobalUserInfo();
  let { isSelectedFollow, userId} = parms;
  // 1：关注 0：取消关注
  if (isSelectedFollow === 1 || isSelectedFollow) {
    var url = '/user/unFollow?userId=' + userId + '&fanId=' + user.id;
  } else {
    var url = '/user/follow?userId=' + userId + '&fanId=' + user.id;
  }
  return new Promise((resolve, reject)=>{
    wx.request({
      url: app.serverUrl + url,
      method: 'POST',
      header: {
        'content-type': 'application/json', // 默认值
        'headerUserId': user.id,
        'headerUserToken': user.userToken
      },
      success: function (res) { 
        resolve(res.data)
      }
    })
  })
}
/**
 * 添加/取消视频喜欢
 */
function likeVideoOrNot(parms) {
var user = app.getGlobalUserInfo();
let { videoId, userId, isFav} = parms;
if (isFav === 1) {
  var url = '/video/userUnLike?userId=' + user.id + '&videoId=' + videoId + '&videoCreateId=' + userId;
} else {
  var url = '/video/userLike?userId=' + user.id + '&videoId=' + videoId + '&videoCreateId=' + userId;
}

var serverUrl = app.serverUrl;
wx.request({
  url: serverUrl + url,
  method: 'POST',
  header: {
    'content-type': 'application/json', // 默认值
    'headerUserId': user.id,
    'headerUserToken': user.userToken
  },
  success: function (res) {}
})
}
/**
 * 上传视频
 */
function uploadVideo(videoMessage) {
  var serverUrl = app.serverUrl;
  var user = app.getGlobalUserInfo();
  let { tmpVideoUrl, bgmId, desc, videoSeconds, tmpHeight, tmpWidth, jurisdictionCode} = videoMessage;
  wx.showLoading({
    title: '上传中...',
  })
  return new Promise((resolve, reject)=>{
    wx.uploadFile({
      url: serverUrl + '/video/uploadVideo',
      formData: {
        userId: user.id,    // fixme 原来的 app.userInfo.id
        bgmId: bgmId,
        desc: desc,
        videoSeconds: videoSeconds,
        videoHeight: tmpHeight,
        videoWidth: tmpWidth,
        jurisdictionCode
      },
      filePath: tmpVideoUrl,
      name: 'file',
      header: {
        'headerUserId': user.id,
        'headerUserToken': user.userToken
      },
      success: function (res) {
        var data = JSON.parse(res.data);
        wx.hideLoading();
        if (data.status == 200) {
          wx.showToast({
            title: '上传成功!~~',
            icon: "success",
            duration: 2000
          });
          resolve(true);
        } else {
          wx.showToast({
            image: "../../images/warning.png",
            title: data.msg,
            icon: "warning",
            duration: 2000
          });
          reject(false);
        }
      },
      fail: function () {
        wx.hideLoading();
        wx.showToast({
          title: '上传失败!',
          image: "../../images/warning.png",
          duration: 2000
        });
        reject(false);
      }
    })
  })
}

function getUserMessage(parm){
  var serverUrl = app.serverUrl;
  var user = app.getGlobalUserInfo();
  var { queryUserId, queryFanId} = parm;
  wx.showLoading({
    title: '加载中...',
  })
  return new Promise((resolve, reject) => {
    wx.request({
      url: serverUrl + '/user/query?userId=' + queryUserId + "&fanId=" + queryFanId,
      method: "POST",
      header: {
        'content-type': 'application/json', // 默认值
        'headerUserId': user.id,
        'headerUserToken': user.userToken
      },
      success: function (res) {
        wx.hideLoading();
        if (res.data.status == 200 || res.data.status == '200') {
          var userInfo = res.data.data;
          var faceImage = "../../images/noneface.png";
          if (userInfo.faceImage == null && userInfo.faceImage == '') {
            userInfo.faceImage = faceImage;
          }
          resolve(userInfo)
        } else if (res.data.status == 500 || res.data.status == '500') {
          wx.showToast({
            title: res.data.msg,
            duration: 3000,
            icon: "none",
            success: function () {
              reject()
            }
          })
        }
      },
      fail: function(){
        reject()
      }
    })
  })
}

function getMyVideoList(userId, page) {
  // 查询视频信息
  wx.showLoading();
  // 调用后端
  var serverUrl = app.serverUrl;
  let loginUserId = app.getGlobalUserInfo().id;
  return new Promise((resolve, reject) => {
    wx.request({
      url: serverUrl + '/video/showAll?loginUserId=' + loginUserId +'&page=' + page + '&pageSize=30',
      method: "POST",
      data: {
        userId: userId
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        wx.hideLoading();
        resolve(res.data.data)
      },
      fail:()=>{
        reject()
      }
    })
  })
}

function getMyLikesList(userId, page) {
  // 查询视频信息
  wx.showLoading();
  // 调用后端
  var serverUrl = app.serverUrl;
  let loginUserId = app.getGlobalUserInfo().id;
  return new Promise((resolve, reject) => {
    wx.request({
      url: serverUrl +
        '/video/showMyLike?loginUserId=' + loginUserId +'&userId=' + userId + '&page=' + page + '&pageSize=30',
      method: "POST",
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        wx.hideLoading();
        resolve(res.data.data)
      },
      fail: () => {
        reject()
      }
    })
  })
}

function getMyFollowList(userId,page) {
  // 查询视频信息
  wx.showLoading();
  // 调用后端
  var serverUrl = app.serverUrl;
  let loginUserId = app.getGlobalUserInfo().id;
  return new Promise((resolve, reject) => {
    wx.request({
      url: serverUrl +
        '/video/showMyFollow?loginUserId=' + loginUserId +'&userId=' + userId + '&page=' + page + '&pageSize=30',
      method: "POST",
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        wx.hideLoading();
        resolve(res.data.data)
      },
      fail: () => {
        reject()
      }
    })
  })
}
/**
   * 分享
   */
function shareMe (videoMessage, videoPath) {
  let me = this;
  var user = app.getGlobalUserInfo();
  let { videoId, userId } = videoMessage;
  wx.showActionSheet({
    itemList: ['下载到本地', '举报用户', '分享给好友', '分享到朋友圈', '分享到QQ空间', '分享到微博'],
    success: function (res) {
      if (res.tapIndex == 0) {
        // 下载
        wx.showLoading({
          title: '后台下载中...',
          duration: 3000
        })
        wx.downloadFile({
          url: videoPath,
          success: function (res) {
            // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
            if (res.statusCode === 200) {
              wx.saveVideoToPhotosAlbum({
                filePath: res.tempFilePath,
                success: function (res) {
                  wx.hideLoading();
                  wx.showToast({
                    title: '下载成功!',
                    icon: "success",
                    duration: 2000
                  })
                },
                fail: function (err) {
                  wx.hideLoading();
                  wx.showToast({
                    title: '下载失败!',
                    image: "../../images/warning.png",
                    duration: 2000
                  })
                }
              })
            } else {
              wx.hideLoading();
              wx.showToast({
                title: '下载失败!',
                image: "../../images/warning.png",
                duration: 2000
              })
            }
          },
          fail: function (err) {
            wx.hideLoading();
            wx.showToast({
              title: '下载失败!',
              image: "../../images/warning.png",
              duration: 2000
            })
          }
        })
      } else if (res.tapIndex == 1) {
        // 举报
        console.log("举报")
        wx.redirectTo({
          url: '/pages/report/report?publishUserId=' + user.id + '&videoId=' + videoId,
        })
      } else if (res.tapIndex == 2) {
        wx.showToast({
          title: '点击上方...按钮转发',
          icon: "none",
          duration: 2000
        })
      } else {
        wx.showToast({
          title: '正在努力开发中...',
          image: "../../images/warning.png",
        })
      }
    }
  })
}
module.exports = {
  getAllVideoList,
  likeVideoOrNot,
  followMe,
  uploadVideo,
  getUserMessage,
  getMyVideoList,
  getMyLikesList,
  getMyFollowList,
  shareMe,
}